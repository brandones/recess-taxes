#!env/bin/python
""" concatenate_pdfs.py

    Copyright 2015 Socos LLC
"""

import argparse
from datetime import datetime
import logging
from PyPDF2 import PdfFileMerger, PdfFileReader
from PyPDF2.utils import PdfReadError
import os
import traceback


LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)
handler = logging.StreamHandler()
LOG.addHandler(handler)

def main(input_dirs, output_file):
    LOG.debug('Input dirs: {}'.format(input_dirs))
    # First we build a dict containing the paths to PDF files we want to
    # concatenate, indexed by the date (as found in the filename
    files_to_concatenate = {}
    for dir_name in input_dirs:
        # "Walk" through the directory tree under dir_name to find PDFs
        for root, dirs, files in os.walk(dir_name):
            amount_in_dir = 0
            # Add all the PDF files we can find
            for f in files:
                if f.endswith(".pdf"):
                    pdf_path = os.path.join(root, f)
                    LOG.debug('Found PDF file {}'.format(pdf_path))
                    # Break filename into components
                    name_parts = f.split('_')
                    # Extract amount and add to running sum for directory
                    amount_in_dir += int(float(name_parts[0]))
                    # Extract date from filename
                    date_string = name_parts[1][:6]
                    pdf_date = datetime.strptime(date_string, '%m%d%y')
                    files_to_concatenate[pdf_date] = pdf_path
            # Print the amount
            LOG.info('{}\tTOTAL AMOUNT FOR {}'.format(amount_in_dir, root))


    # Okay! Now files_to_concatenate has a buncha filenames. Let's actually concatenate them.
    merger = PdfFileMerger()
    for item_date, filename in files_to_concatenate.items():
        try:
            merger.append(PdfFileReader(file(filename, 'rb')))
        except PdfReadError:
            LOG.error('Fucked up trying to read {}. Skipping.'.format(filename))
    try:
        merger.write(output_file)
    except PdfReadError, e:
        LOG.error('Fucked up trying to write the file. Shit shit shit shi')
        tb = traceback.format_exc()
        LOG.error(tb)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_dirs', metavar='Input Directories', nargs='+')
    parser.add_argument('--output', default='concatenated_pdfs.pdf')
    args = parser.parse_args()
    main(args.input_dirs, args.output)
